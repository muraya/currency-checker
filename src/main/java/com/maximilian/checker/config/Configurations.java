package com.maximilian.checker.config;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@Getter
@Slf4j
public class Configurations {

    // Currency Configurations

    @Value("${currency.btc.url}")
    private String btcCurrencyUrl;

    @Value("${currency.default.url}")
    private String defaultCurrencyUrl;

    // Telegram Configuration
    @Value("${bot.name}")
    private String botName;

    @Value("${bot.token}")
    private String botToken;

    @Value("${access.pwd}")
    private String password;

    @Value("${zone.id}")
    private String desiredZoneId;

    @Value("${general.ids}")
    private List<String> chatIds;

    //scheduler configs
    @Value("${app.url}")
    private String appUrl;

    @Value("${greetings.morning}")
    private String greetingsMorning;

    @Value("${greetings.evening}")
    private String greetingsEvening;

    private Map<String, String> morningGreetingsByChatId;

    private Map<String, String> eveningGreetingsByChatId;

    public Map<String, String> getMorningGreetingByChatIdMap() {
        synchronized (this) {
            if(morningGreetingsByChatId == null) {
                morningGreetingsByChatId = getGreetingByChatIdMap(greetingsMorning);
            }
        }
        return new HashMap<>(morningGreetingsByChatId);
    }

    public Map<String, String> getEveningGreetingByChatIdMap() {
        synchronized (this) {
            if(eveningGreetingsByChatId == null) {
                eveningGreetingsByChatId = getGreetingByChatIdMap(greetingsEvening);
            }
        }
        return new HashMap<>(eveningGreetingsByChatId);
    }

    private Map<String, String> getGreetingByChatIdMap(String greetingLine) {
        String[] split = greetingLine.split("\\|");
        Map<String, String> result = new HashMap<>();
        for(String pair : split) {
            String[] pairSplit = pair.split(":");
            if(pairSplit.length > 1) {
                result.put(pairSplit[0], pairSplit[1]);
            } else {
                log.warn("Invalid pair '" + pair + "' to parse");
            }
        }
        return result;
    }



}
