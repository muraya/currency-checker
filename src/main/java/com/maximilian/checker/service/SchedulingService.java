package com.maximilian.checker.service;

import com.maximilian.checker.bot.CurrencyBot;
import com.maximilian.checker.config.Configurations;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.net.http.HttpResponse;

@Service
@Slf4j
public class SchedulingService {

    private final CurrencyBot currencyBot;
    private final HttpClientWrapper httpClientWrapper;
    private final Configurations configurations;

    @Autowired
    public SchedulingService(CurrencyBot currencyBot, HttpClientWrapper httpClientWrapper, Configurations configurations) {
        this.currencyBot = currencyBot;
        this.httpClientWrapper = httpClientWrapper;
        this.configurations = configurations;
    }

    // 10:00 every day
    @Scheduled(cron = "0 0 10 * * *", zone = "Europe/Moscow")
    private void notifyWithCurrenciesMorning() {
        currencyBot.sendCurrenciesTo(configurations.getChatIds(), configurations.getMorningGreetingByChatIdMap());

    }

    // 22:00 every day
    @Scheduled(cron = "0 0 22 * * *", zone = "Europe/Moscow")
    private void notifyWithCurrenciesEvening() {
        currencyBot.sendCurrenciesTo(configurations.getChatIds() , configurations.getEveningGreetingByChatIdMap());
    }

    // every 5 minutes, is used to prevent heroku from putting application to sleep mode,
    @Scheduled(cron = "0 */5 * * * *", zone = "Europe/Moscow")
    private void pingSelf() {
        try {
            HttpResponse<String> responseFromUrl = httpClientWrapper.getResponseFromUrl(configurations.getAppUrl(),  HttpResponse.BodyHandlers.ofString());
            log.info("Heartbeat response: " + responseFromUrl.body());
        } catch (Exception ex) {
            log.warn("Error while requesting heartbeat: " + ex.getMessage());
        }
    }

}
