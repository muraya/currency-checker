package com.maximilian.checker.service;

import com.maximilian.checker.config.Configurations;
import com.maximilian.checker.data.CryptoCurrencyList;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CryptoCurrencyService {


    @Autowired
    protected final Configurations configurations;

    @Autowired
    protected final HttpClientWrapper httpClientWrapper;
    public CryptoCurrencyList getBitcoinCurrency() {
        return httpClientWrapper.get(configurations.getBtcCurrencyUrl(), CryptoCurrencyList.class);
    }

}
