package com.maximilian.checker.service;

import com.maximilian.checker.config.Configurations;
import com.maximilian.checker.data.CBRGeneralObject;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CurrencyService{


    @Autowired
    protected final Configurations configurations;

    @Autowired
    protected final HttpClientWrapper httpClientWrapper;

    public CBRGeneralObject getCBRCurrency() {
        return httpClientWrapper.get(configurations.getDefaultCurrencyUrl(), CBRGeneralObject.class);
    }

}
