package com.maximilian.checker.exception;

public class HttpException extends GeneralException {

    public HttpException(String msg) {
        super(msg);
    }

}
