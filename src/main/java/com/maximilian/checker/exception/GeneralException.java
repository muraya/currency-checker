package com.maximilian.checker.exception;

public class GeneralException extends RuntimeException {

    public GeneralException(String message) {
        super(message);
    }

}
